import { NgModule,CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { AddSalesInvoicePage } from './add-sales-invoice.page';
import { MaterialModule } from '../../material/material.module';
import { InlineEditComponent } from './inline-edit/inline-edit.component';
import { KeyDownDetectorDirective } from './on-key-down-directive';
import { AppCommonModule } from '../../common/app-common.module';
import { CustomerCreateDialogComponent } from './customer-create-dialog/customer-create-dialog.component';
import { DraftListComponent } from './draft-list/draft-list.component';
import { PaymentDialogueComponent } from './payment-dialogue/payment-dialogue.component';
import { ItemSearchComponent } from './item-search/item-search.component';

const routes: Routes = [
  {
    path: '',
    component: AddSalesInvoicePage,
  },
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    AppCommonModule,
    IonicModule,
    RouterModule.forChild(routes),
    MaterialModule,
    ReactiveFormsModule,
  ],
  schemas : [
    CUSTOM_ELEMENTS_SCHEMA
  ],
  declarations: [
    AddSalesInvoicePage,
    InlineEditComponent,
    KeyDownDetectorDirective,
    CustomerCreateDialogComponent,
    DraftListComponent,
    PaymentDialogueComponent,
    ItemSearchComponent
  ],
  exports: [InlineEditComponent, KeyDownDetectorDirective,CustomerCreateDialogComponent, DraftListComponent,PaymentDialogueComponent,ItemSearchComponent],
})
export class AddSalesInvoicePageModule {}
