import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';


import { PaymentDialogueComponent } from './payment-dialogue.component';
import { MaterialModule } from 'src/app/material/material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

describe('PaymentDialogueComponent', () => {
  let component: PaymentDialogueComponent;
  let fixture: ComponentFixture<PaymentDialogueComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PaymentDialogueComponent ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      //imports: [IonicModule.forRoot()]
      imports: [
        MaterialModule,
        FormsModule,
        ReactiveFormsModule,
        BrowserAnimationsModule,
        IonicModule
      ]
    }).compileComponents();

    fixture = TestBed.createComponent(PaymentDialogueComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
