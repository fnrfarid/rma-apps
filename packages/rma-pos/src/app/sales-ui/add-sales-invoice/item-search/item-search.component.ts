import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import {
  FormControl,
  FormGroup,
  FormArray
} from '@angular/forms';

import { AddSalesInvoicePage } from '../add-sales-invoice.page';
import { ItemPriceService } from '../../services/item-price.service';
import { SalesService } from '../../services/sales.service';
@Component({
  selector: 'app-item-search',
  templateUrl: './item-search.component.html',
  styleUrls: ['./item-search.component.scss'],
})
export class ItemSearchComponent implements OnInit {
  searchData: any []=[];

  salesInvoiceItemsForm = new FormGroup({
    items: new FormArray([]),
    total: new FormControl(0),
    filterKey: new FormControl('Item Code'),
    filterValue: new FormControl(),
    filterName: new FormControl(),
  });


  public dialog: MatDialog
  constructor( private dialogRef: MatDialogRef<AddSalesInvoicePage>,
    private itemPriceService: ItemPriceService,
    private salesService: SalesService,
    @Inject (MAT_DIALOG_DATA) public data : any) {
    
      // this.searchData=data['searchdata'];
    }

  ngOnInit() {
    this.salesInvoiceItemsForm.valueChanges.subscribe((data)=>{
      if(data.filterKey!= ""){
        var filter = JSON.stringify({item_name : data.filterValue})
      }else{
        var filter = JSON.stringify({})
      }
      this.itemPriceService.findItems(filter ,JSON.stringify({ name: 'asc' }),0,10).subscribe((data)=>{
        
        this.searchData = data.docs
        this.searchData.forEach((element : any,index,modified_arr) => {
          this.salesService.getImageList(element.name).subscribe((data:any)=>{
            if(element.hasOwnProperty("website_image")){
              element.website_image = data.data.image
            } else{
              this.searchData[index]['website_image']= data.data.image
            }
          })
        })
      })
      if(this.searchData.length == 0){
        if(data.filterKey!= ""){
          var filter = JSON.stringify({ item_code : data.filterValue})
        }else{
          var filter = JSON.stringify({})
        }
      this.itemPriceService.findItems(filter ,JSON.stringify({ name: 'asc' }),0,10).subscribe((data)=>{
        
        this.searchData = data.docs

        this.searchData.forEach((element : any,index,modified_arr) => {
          this.salesService.getImageList(element.name).subscribe((data:any)=>{
            if(element.hasOwnProperty("website_image")){
              element.website_image = data.data.image
            } else{
              this.searchData[index]['website_image']= data.data.image
            }
          })
        })
      })
    }
    else if(this.searchData.length == 0){
      if(data.filterKey!= ""){
        var filter = JSON.stringify({ barcode : data.filterValue})
      }else{
        var filter = JSON.stringify({})
      }
    this.itemPriceService.findItems(filter ,JSON.stringify({ name: 'asc' }),0,10).subscribe((data)=>{
      
      this.searchData = data.docs
      this.searchData.forEach((element : any,index,modified_arr) => {
        this.salesService.getImageList(element.name).subscribe((data:any)=>{
          if(element.hasOwnProperty("website_image")){
            element.website_image = data.data.image
          } else{
            this.searchData[index]['website_image']= data.data.image
          }
        })
      })
    })
  }
  else if(this.searchData.length == 0){
    if(data.filterKey!= ""){
      var filter = JSON.stringify({ item_group : data.filterValue})
    }else{
      var filter = JSON.stringify({})
    }
  this.itemPriceService.findItems(filter ,JSON.stringify({ name: 'asc' }),0,10).subscribe((data)=>{
    this.searchData = data.docs
    this.searchData.forEach((element : any,index,modified_arr) => {
      this.salesService.getImageList(element.name).subscribe((data:any)=>{
        if(element.hasOwnProperty("website_image")){
          element.website_image = data.data.image
        } else{
          this.searchData[index]['website_image']= data.data.image
        }
      })
    })
  })
}
    })



    
  }


  closeDialog() {
    this.dialogRef.close()
  };
}
